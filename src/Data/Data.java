/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.util.ArrayList;

/**
 *
 * @author c3356
 */
public class Data {

    public static ArrayList<User> userlist = new ArrayList<User>();
    public static String user;
    public static String password;

    public static String getUser() {
        return user;
    }

    public static void setUser(String userlog) {
        Data.user = userlog;
    }

    public static String getPasswordlog() {
        return password;
    }

    public static void setPasswordlog(String passwordlog) {
        Data.password = passwordlog;
    }

    public static void load() {

        User p1 = new User(01, "Admin01", "1234", "Sorawit", "Surname", 19, "0899999999", 55, 171);
        User p2 = new User(02, "Member01", "23456", "Sirawit", "Surname", 19, "0899999999", 55, 171);
        userlist.add(p1);
        userlist.add(p2);

    }

    public static void setlog(String userlog, String passwordlog) {
        Data.user = userlog;
        Data.password = passwordlog;

    }
}
